package com.example.chat07;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Post {

    private String content;
    private String login;
    private String date;
    private String id;

    public Post(String content, String login) {
        this.content = content;
        this.login = login;
    }

    public String getContent() {
        return content;
    }

    public String getLogin() {
        return login;
    }

    public String getDate() {
        return date;
    }

    public String getId() {
        return id;
    }
}





