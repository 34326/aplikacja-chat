package com.example.chat07;

public class Recycle {
    private String userLogin;
    private String userMessage;
    private String userId;
    private String userdate;

    public Recycle(String login, String content, String id, String date){
        userLogin = login;
        userMessage = content;
        userId = id;
        userdate = date;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserdate() {
        return userdate;
    }
}
