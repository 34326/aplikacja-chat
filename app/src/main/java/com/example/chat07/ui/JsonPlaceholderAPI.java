package com.example.chat07.ui;

import com.example.chat07.Post;

import java.util.List;


import retrofit2.Call;
import retrofit2.http.Body;

import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface JsonPlaceholderAPI {
    @GET("shoutbox/messages?last=20")
    Call<List<Post>> getPosts();

    @POST("shoutbox/message")
    Call<Post>createPost(@Body Post post);

    @DELETE("shoutbox/message/{id}")
    Call<Void>deletePost(@Path("id") String id);

    @PUT("shoutbox/message/{id}")
    Call<Post> putPost(@Path("id") String id, @Body Post post);

}
