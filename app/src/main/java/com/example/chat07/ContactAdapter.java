package com.example.chat07;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

    private EditListener EditListener;
    public ArrayList<Recycle> mRecycle;

    public ContactAdapter(ArrayList<Recycle>examplemess, EditListener EditListener){
        mRecycle = examplemess;
        this.EditListener = EditListener;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comment, viewGroup, false);

        ContactViewHolder cvh = new ContactViewHolder(v, EditListener);
        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder contactViewHolder, int i) {
        Recycle currentItem = mRecycle.get(i);

        contactViewHolder.eLogin.setText(currentItem.getUserLogin());
        contactViewHolder.eText.setText(currentItem.getUserMessage());
        contactViewHolder.eDate.setText(currentItem.getUserdate());
    }

    @Override
    public int getItemCount() {
        return mRecycle.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView eLogin;
        public TextView eText;
        public TextView eDate;
        EditListener EditListener;

        public ContactViewHolder(@NonNull View itemView, EditListener EditListener) {
            super(itemView);
            eDate = itemView.findViewById(R.id.message3);
            eLogin = itemView.findViewById(R.id.message2);
            eText = itemView.findViewById(R.id.message1);
            this.EditListener = EditListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            EditListener.EditClick(getAdapterPosition());
        }
    }

    public interface EditListener{
        void EditClick(int position);
    }
}
