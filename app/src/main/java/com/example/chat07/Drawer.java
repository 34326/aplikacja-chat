package com.example.chat07;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.Menu;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chat07.ui.JsonPlaceholderAPI;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.chat07.databinding.ActivityDrawerBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Drawer extends AppCompatActivity implements ContactAdapter.EditListener {
    SwipeRefreshLayout refreshLayout;
    private AppBarConfiguration mAppBarConfiguration;
    private ActivityDrawerBinding binding;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView mRecycleView;
    private EditText editText;
    private JsonPlaceholderAPI jsonPlaceholderAPI;
    private String idEdit;
    private String loginEdit;
    private String contentEdit;
    private String intentActivity;

    private String idDelete;

    private String login;

    private ArrayList<Recycle> exampleList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://tgryl.pl/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI.class);

        binding = ActivityDrawerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appBarDrawer.toolbar);

        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_settings)
                .setOpenableLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_drawer);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        mLayoutManager = new LinearLayoutManager(this);

        Intent intent = getIntent();
        intentActivity = intent.getStringExtra("ACTIVITY");

        if(intentActivity.equals("MainActivity"))
        {
            login = intent.getStringExtra("KEY_YOUR_LOGIN");
        }

        if(intentActivity.equals("Edit"))
        {
            idEdit = intent.getStringExtra("IDEDIT");
            loginEdit = intent.getStringExtra("LOGINEDIT");
            contentEdit = intent.getStringExtra("CONTENTEDIT");

            login = loginEdit;

            updatePost(idEdit, loginEdit, contentEdit);
        }

        if(intentActivity.equals("Delete"))
        {
            idDelete = intent.getStringExtra("IDDELETE");
            loginEdit = intent.getStringExtra("LOGINDELETE");
            login = loginEdit;

            deletePost(idDelete);
        }

        refreshLayout=findViewById(R.id.refreshLayout);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadcomment();
                refreshLayout.setRefreshing(false);
            }
        });
        loadcomment();
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        loadcomment();
                        Thread.sleep(60000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadcomment();
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        thread.start();
    }

    public void loadcomment(){
        Call<List<Post>> call = jsonPlaceholderAPI.getPosts();
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if (!response.isSuccessful()){

                    return;
                }
                List<Post> posts = response.body();
                Collections.reverse(posts);

                exampleList.clear();
                for (Post post : posts){
                    exampleList.add(new Recycle(post.getLogin(), post.getContent(), post.getId(), post.getDate()));
                    mRecycleView = findViewById(R.id.recycleView);
                    mRecycleView.setHasFixedSize(true);

                    mAdapter = new ContactAdapter(exampleList, Drawer.this);
                    mRecycleView.setLayoutManager(mLayoutManager);
                    new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecycleView);
                    mRecycleView.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
            }
        });
    }

    public void createPost(View view){
        editText = findViewById(R.id.newMessage);
        Intent intent = getIntent();
        String text = intent.getStringExtra("KEY_YOUR_LOGIN");
        Post post = new Post(editText.getText().toString(),text);
        Call<Post> call = jsonPlaceholderAPI.createPost(post);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                loadcomment();
                if (!response.isSuccessful()){

                    return;
                }
                Post postResponse = response.body();

            }
            @Override
            public void onFailure(Call<Post> call, Throwable t) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }


    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_drawer);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void deleteLogin(MenuItem item) {
        String yLogin = null;
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("LOGIN_KEY", yLogin);
        editor.apply();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void EditClick(int position) {
        if(exampleList.get(position).getUserLogin().equals(login)) {
            Intent intent = new Intent(this, Edit.class);
            intent.putExtra("idEdit", exampleList.get(position).getUserId());
            intent.putExtra("loginEdit", exampleList.get(position).getUserLogin());
            intent.putExtra("loginToCheck", login);
            intent.putExtra("contentEdit", exampleList.get(position).getUserMessage());
            startActivity(intent);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Niepoprawny login",Toast.LENGTH_SHORT).show();
        }
    }

    public void updatePost(String idEdit, String loginEdit, String contentEdit){
        Post post = new Post(contentEdit,loginEdit);
        Call<Post> call = jsonPlaceholderAPI.putPost(idEdit, post);

        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                loadcomment();
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                loadcomment();
            }
        });
    }

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback =
            new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
                @Override
                public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                    int i = viewHolder.getAdapterPosition();

                    String loginToTest = exampleList.get(i).getUserLogin();

                    if(loginToTest.equals(login))
                    {
                        String id = exampleList.get(i).getUserId();

                        Call<Void> call = jsonPlaceholderAPI.deletePost(id);

                        call.enqueue(new Callback<Void>() {
                            @Override
                            public void onResponse(Call<Void> call, Response<Void> response) {
                                loadcomment();
                            }

                            @Override
                            public void onFailure(Call<Void> call, Throwable t) {
                                loadcomment();
                            }

                        });
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Niepoprawny login",Toast.LENGTH_SHORT).show();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            };

    public void deletePost(String id) {
        Call<Void> call = jsonPlaceholderAPI.deletePost(id);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                loadcomment();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                loadcomment();
            }
        });
    }
}