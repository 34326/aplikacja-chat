package com.example.chat07;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Edit extends AppCompatActivity {

    private String idEditToSend;
    private String loginEditToSend;
    private String contentEditToSend;

    private String idEdit;
    private String loginEdit;
    private String contentEdit;
    private EditText editText;
    private TextView showOld;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        TextView idView = findViewById(R.id.idView);
        TextView loginView = findViewById(R.id.loginView);
        editText = findViewById(R.id.editText1);
        showOld = findViewById(R.id.showOld);

        Intent intent = getIntent();

        idEdit = intent.getStringExtra("idEdit");
        loginEdit = intent.getStringExtra("loginEdit");
        contentEdit = intent.getStringExtra("contentEdit");

        showOld.setText(contentEdit);

        idView.setText(idEdit);
        loginView.setText(loginEdit);
        editText.setText(contentEdit);

        contentEditToSend = editText.getText().toString();
        idEditToSend = idEdit;
        loginEditToSend = loginEdit;

    }

    public void sendEdit(View view)
    {
        contentEditToSend = editText.getText().toString();
        idEditToSend = idEdit;
        loginEditToSend = loginEdit;

        Intent intent = new Intent(this,Drawer.class);
        intent.putExtra("IDEDIT", idEditToSend);
        intent.putExtra("LOGINEDIT", loginEditToSend);
        intent.putExtra("CONTENTEDIT", contentEditToSend);
        intent.putExtra("ACTIVITY", "Edit");

        startActivity(intent);
    }

    public void sendDelete(View view)
    {
        Intent intent = new Intent(this,Drawer.class);
        intent.putExtra("IDDELETE", idEditToSend);
        intent.putExtra("LOGINDELETE", loginEdit);
        intent.putExtra("ACTIVITY", "Delete");

        startActivity(intent);
    }
}