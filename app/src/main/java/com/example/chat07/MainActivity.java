package com.example.chat07;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText loginApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loginApp = findViewById(R.id.login);
        if (isConnected()) {
            String log = loadData();
            if (log != "") {
                sendLogin(loginApp);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Brak połączenia z internetem", Toast.LENGTH_SHORT).show();
        }
    }

    private String loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        String userLogin = sharedPreferences.getString("LOGIN_KEY", "");
        loginApp.setText(userLogin);
        return userLogin;
    }

    private void saveUserLogin(String yourLogin) {
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("LOGIN_KEY", yourLogin);
        editor.apply();
    }

    public void sendLogin(View view) {
        if (isConnected()) {
            String yourLogin = loginApp.getText().toString();
            if (yourLogin.matches("")){
                Toast.makeText(getApplicationContext(), "Nie podano loginu", Toast.LENGTH_SHORT).show();
            }
            else {
                Intent intent = new Intent(this, Drawer.class);
                intent.putExtra("ACTIVITY", "MainActivity");
                intent.putExtra("KEY_YOUR_LOGIN", yourLogin);
                saveUserLogin(yourLogin);
                startActivity(intent);
            }
        }
        else{
            Toast.makeText(getApplicationContext(), "Brak połączenia z internetem", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isConnected() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null) {
            if (networkInfo.isConnected()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}